package com.example.chatapp.ui.userList.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chatapp.databinding.UserListViewBinding;
import com.example.chatapp.ui.chatRoom.GroupChatRoom;
import com.example.chatapp.ui.chatRoom.adapter.PersonalChatActivity;
import com.example.chatapp.ui.chatRoom.model.UserListModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder> {
    private Context context;
    private List<UserListModel> userList ;
    private String Username;
    private OpenUserList openUserList;
    public UserListAdapter(Context context) {
        this.context = context;
        userList = new ArrayList<>();
    }

    public void setUserList(List<UserListModel> userList) {
        if(userList.size() >0)
            this.userList = userList;
    }



    public interface OpenUserList
    {
        void OpenUser(UserListModel userListModel);
    }
    public void setOpenUserList(OpenUserList openUserList)
    {
        this.openUserList = openUserList;
    }


    @NonNull
    @NotNull
    @Override
    public UserListAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        UserListViewBinding binding = UserListViewBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false);
        return new  UserListAdapter.ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull UserListAdapter.ViewHolder holder, int position) {
        holder.binding.userName.setText(userList.get(position).getUsername());
        holder.binding.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openUserList.OpenUser(userList.get(position));
               // AppCompatActivity activity = (AppCompatActivity)v.getContext();


            }
        });

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public void setUserName(String userName) {
        Username = userName;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
      private UserListViewBinding binding;
        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            binding = UserListViewBinding.bind(itemView);
        }

        @Override
        public void onClick(View v) {

        }
    }
}
