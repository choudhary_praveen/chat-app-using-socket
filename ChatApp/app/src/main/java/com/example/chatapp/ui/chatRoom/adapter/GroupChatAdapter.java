package com.example.chatapp.ui.chatRoom.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chatapp.R;
import com.example.chatapp.common.Constants;
import com.example.chatapp.databinding.ChatNewMemberBinding;
import com.example.chatapp.databinding.ChatReciverBinding;
import com.example.chatapp.databinding.ChatSenderBinding;
import com.example.chatapp.ui.chatRoom.model.ChatModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class GroupChatAdapter extends RecyclerView.Adapter<GroupChatAdapter.ViewHolder> {

    private Context context;
    private List<ChatModel> msgList;


    public GroupChatAdapter(Context context) {
        this.context = context;
        msgList = new ArrayList<>();
    }

    public void setMessageList(List<ChatModel> chatModels)
    {
        if(chatModels!= null&&chatModels.size()>0)
        {
            msgList.addAll(chatModels);
        }
    }
    public void setMessageModel(ChatModel chatModel)
    {
        msgList.add(chatModel);

    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case 0:
                view = LayoutInflater.from(context).inflate(R.layout.chat_new_member, parent, false);
                break;
            case 1:
                view = LayoutInflater.from(context).inflate(R.layout.chat_sender, parent, false);
                break;
            default:
                view = LayoutInflater.from(context).inflate(R.layout.chat_reciver, parent, false);
                break;
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull GroupChatAdapter.ViewHolder holder, int position) {

        switch (getItemViewType(position)) {
            case 0:
                holder.setContentNew(position);
                break;
            case 1:
                holder.setContentSender(position);
                break;
            case 2:
                holder.setContentReceiver(position);
                break;
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (msgList.get(position).getMessage().equalsIgnoreCase("!@#$")) {
            return 0;
        } else if (msgList.get(position).getUuid().equals(Constants.SENDER_UUID)) {
            return 1;
        } else {
            return 2;
        }
    }

    @Override
    public int getItemCount() {
        return msgList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        ChatNewMemberBinding newMemberBinding;
        ChatReciverBinding reciverBinding;
        ChatSenderBinding senderBinding;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
        public void setContentNew(int pos) {
            if (newMemberBinding == null) {
                newMemberBinding = ChatNewMemberBinding.bind(itemView);
            }
           newMemberBinding.txtMessages.setText("Member Added : "+ msgList.get(pos).getUsername());
        }

        public void setContentSender(int pos) {
            if (senderBinding == null) {
                senderBinding = ChatSenderBinding.bind(itemView);
            }
            senderBinding.txtMessages.setText(msgList.get(pos).getMessage());

        }

        public void setContentReceiver(int pos) {
            if (reciverBinding == null) {
                reciverBinding = ChatReciverBinding.bind(itemView);
            }
            reciverBinding.txtMessages.setText(msgList.get(pos).getMessage());

        }
    }
}
