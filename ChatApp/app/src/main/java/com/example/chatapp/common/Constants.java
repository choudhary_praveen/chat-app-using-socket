package com.example.chatapp.common;

import java.util.UUID;

public class Constants {
    public static String CLOSE_SERVER = "close-SERVER";
    public static String IP_ADDRESS = "192.168.241.33";
    public static Integer PORT_NO = 6667;
    public static UUID SENDER_UUID = UUID.randomUUID();
}
