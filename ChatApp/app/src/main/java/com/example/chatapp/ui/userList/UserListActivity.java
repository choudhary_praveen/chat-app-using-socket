package com.example.chatapp.ui.userList;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.chatapp.common.Constants;
import com.example.chatapp.databinding.ActivityUserListBinding;
import com.example.chatapp.socket.IMessageNotifier;
import com.example.chatapp.socket.SocketHandler;
import com.example.chatapp.socket.SocketInterface;
import com.example.chatapp.ui.chatRoom.GroupChatRoom;
import com.example.chatapp.ui.chatRoom.adapter.PersonalChatActivity;
import com.example.chatapp.ui.chatRoom.model.ChatModel;
import com.example.chatapp.ui.chatRoom.model.UserListModel;
import com.example.chatapp.ui.userList.adapter.UserListAdapter;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class UserListActivity extends AppCompatActivity implements IMessageNotifier , UserListAdapter.OpenUserList {
    private ActivityUserListBinding binding;
    private String UserName="";
     private HashMap<UUID,String> userListMap;
    private List<UserListModel> userList;
    private UserListAdapter adapter;
    private ArrayList<ChatModel> msgList;
    public static HashMap<UUID,List<ChatModel>> listHashMap;
    SocketInterface socketInterface;
    Gson gson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityUserListBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initialize();
    }

    private void initialize() {
        listHashMap = new HashMap<>();
        msgList = new ArrayList<>();
        userListMap = new HashMap<>();
        userList = new ArrayList<>();
        gson = new Gson();
        adapter = new UserListAdapter(this);
        socketInterface = SocketHandler.getInstance();
        if(getIntent().getStringExtra("username")!=null)
        {
            UserName = getIntent().getStringExtra("username");
            ChatModel model = new ChatModel();
          //  model.setUsername(getIntent().getStringExtra("username"));
            model.setMessage("@#$");
            model.setUuid(Constants.SENDER_UUID);

            String jsonString = gson.toJson(model, ChatModel.class);
            socketInterface.sendMessages(jsonString);



        }
        if(UserName.length()>0)
        {
            ChatModel model1 = new ChatModel();

            model1.setUsername(UserName);
            model1.setMessage("!@#$");
            //  model.setSender(false);
            model1.setUuid(Constants.SENDER_UUID);
            Gson gson = new Gson();
            String jsonString1 = gson.toJson(model1, ChatModel.class);
            socketInterface.sendMessages(jsonString1);
        }
        if(getIntent().getParcelableExtra("msglist")!=null)
        {
            msgList = getIntent().getParcelableExtra("msglist");


        }


        binding.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserListActivity.this, GroupChatRoom.class);
                intent.putExtra("username",UserName);
                intent.putExtra("msglist",msgList);
                startActivityForResult(intent,10001);
            }
        });
        socketInterface.getMessage(this::newMessage);
        binding.userList.setAdapter(adapter);
        adapter.setUserName(UserName);
        adapter.setOpenUserList(this);
        binding.userList.setLayoutManager(new LinearLayoutManager(this));


    }
    @Override
    public void newMessage(String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (message != null ) {
                    Log.d("socketddd",message);
                    ChatModel msgModel = gson.fromJson(message, ChatModel.class);
                    if(msgModel.getMessage().equalsIgnoreCase("@#$"))
                    {
                        if(msgModel.getUserList()!=null)
                        {
                            userListMap = msgModel.getUserList();
                            if(userListMap.size() == 0)
                            {
                                userListMap = msgModel.getUserList();
                            }
                            else {
                                HashMap<UUID ,String> map = msgModel.getUserList();
                                userListMap.putAll(map);
                            }
                            for(Map.Entry<UUID,String> map : userListMap.entrySet())
                            {

                                UserListModel model = new UserListModel();
                                model.setId(map.getKey());
                                model.setUsername(map.getValue());
                                if(!Constants.SENDER_UUID.equals(model.getId()) )
                                {
                                    boolean present = false;
                                    for(UserListModel user : userList)
                                    {
                                        if(user.getId().equals(map.getKey()))
                                        {
                                            present = true;
                                            break;
                                        }
                                    }
                                    if(!present)
                                    userList.add(model);
                                   // adapter.setUserList(userList);
                                   // adapter.notifyDataSetChanged();
                                }
                            }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    adapter.setUserList(userList);
                                    adapter.notifyDataSetChanged();
                                }
                            });
                        }
                    }
                    else if(!msgModel.isPersonal())
                    {
                        if(listHashMap.get(Constants.SENDER_UUID)== null)
                        {
                            listHashMap.put(Constants.SENDER_UUID,new ArrayList<>());
                        }
                        listHashMap.get(Constants.SENDER_UUID).add(msgModel);
                    }
                    else if(msgModel.isPersonal())
                    {
                        if(!Constants.SENDER_UUID.equals(msgModel.getUuid()))
                        {
                            if(listHashMap.get(msgModel.getUuid())==null)
                            {
                                listHashMap.put(msgModel.getUuid(),new ArrayList<>());

                            }
                            listHashMap.get(msgModel.getUuid()).add(msgModel);
                        }
                        else if(!Constants.SENDER_UUID.equals(msgModel.getReceiver()))
                        {
                            if(listHashMap.get(msgModel.getReceiver())==null)
                            {
                                listHashMap.put(msgModel.getReceiver(),new ArrayList<>());

                            }
                            listHashMap.get(msgModel.getReceiver()).add(msgModel);
                        }
                    }

                    System.out.println();
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void OpenUser(UserListModel userListModel) {
        Intent intent = new Intent(getBaseContext(), PersonalChatActivity.class);
        intent.putExtra("receiver",userListModel);
        intent.putExtra("username",UserName);
        startActivityForResult(intent,10001);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 10001 && resultCode == RESULT_OK)
        {
            if(data != null)
            {
                userListMap.putAll((HashMap<UUID,String>)data.getSerializableExtra("userList"));

                for(Map.Entry<UUID,String> map : userListMap.entrySet())
                {

                    UserListModel model = new UserListModel();
                    model.setId(map.getKey());
                    model.setUsername(map.getValue());
                    if(!Constants.SENDER_UUID.equals(model.getId()) )
                    {
                        boolean present = false;
                        for(UserListModel user : userList)
                        {
                            if(user.getId().equals(map.getKey()))
                            {
                                present = true;
                                break;
                            }
                        }
                        if(!present)
                            userList.add(model);
                        // adapter.setUserList(userList);
                        // adapter.notifyDataSetChanged();
                    }
                }
                adapter.setUserList(userList);
                adapter.notifyDataSetChanged();
            }
        }
    }
}