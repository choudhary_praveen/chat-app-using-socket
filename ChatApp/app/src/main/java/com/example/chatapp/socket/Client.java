package com.example.chatapp.socket;

import com.google.gson.Gson;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Client extends Thread {
    private DataInputStream din;
    private DataOutputStream dout;
    private HashMap<UUID, Socket> map;
    private UUID uuid;


    Client(HashMap<UUID, Socket> socket, UUID uuid) throws IOException {
        this.uuid = uuid;
        din = new DataInputStream(socket.get(uuid).getInputStream());
        dout = new DataOutputStream(socket.get(uuid).getOutputStream());
        this.map = socket;
    }

    @Override
    public void run() {
        super.run();
        while (!map.get(uuid).isClosed()) {
            if (din != null) {
                String str = "";
                try {
                    str = din.readUTF();
                    if (str != null) {
                        for (Map.Entry mapElement : map.entrySet()) {
                            Socket socket = (Socket) mapElement.getValue();
                            UUID currentUuid = (UUID) mapElement.getKey();
                            if (!socket.isClosed()) {
                                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                                dataOutputStream.writeUTF(str);
                                dataOutputStream.flush();
                                System.out.println("writing message on server" + str);
                            } else {
                                if (currentUuid != uuid) {
                                    map.remove(currentUuid);
                                }
                                System.out.println("client disconnected");
                            }
                        }
                    }
                    System.out.println("" + str);
                    System.out.println("total clients connected" + map.size());


                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }

            }
        }
        try {
            din.close();
            dout.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
   /* private DataInputStream din;
    private DataOutputStream dout;
    private ArrayList<Socket> sockets;
    private int pos;

    Client(ArrayList<Socket> socket) throws IOException {
        pos = socket.size() - 1;
        din = new DataInputStream(socket.get(pos).getInputStream());
        dout = new DataOutputStream(socket.get(pos).getOutputStream());
        this.sockets = socket;

    }

    @Override
    public void run() {
        super.run();
        while (!sockets.get(pos).isClosed()) {
            if (din != null) {
                String str = "";
                try {
                    str = din.readUTF();
                    if (str != null) {
                        for (int i = 0; i < sockets.size(); i++) {
                            if (!sockets.get(i).isClosed()) {
                                DataOutputStream dataOutputStream = new DataOutputStream(sockets.get(i).getOutputStream());
                                dataOutputStream.writeUTF(str);
                                dataOutputStream.flush();
                            } else {
                                System.out.println("client disconnected");
                            }
                        }
                    }
                    System.out.println(""+str);

                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }

            }
        }
        try {
            din.close();
            dout.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/
}
