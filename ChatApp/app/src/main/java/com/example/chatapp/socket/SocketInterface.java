package com.example.chatapp.socket;

public interface SocketInterface {
     boolean connect();
     boolean disconnect();
     void getMessage(IMessageNotifier messageNotifier);
     void sendMessages(String message);

}
