package com.example.chatapp.socket;

import android.provider.SyncStateContract;
import android.util.Log;

import com.example.chatapp.common.Constants;
import com.example.chatapp.ui.chatRoom.model.ChatModel;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketHandler implements SocketInterface{
    private static Socket socket = null;
    private static SocketInterface socketInterface;
    private Thread connectThread;
    private Thread messageReceiveThread;
    private Thread messageSendThread;

    private String sendMessage;
    private String receiveMessage;
    private DataInputStream inputStream;
    private DataOutputStream outputStream;
    private boolean isConnected = false;
    private BufferedReader bufferedReader;
    IMessageNotifier notifier;
    private SocketHandler()
    {

    }
    public static SocketInterface getInstance()
    {
        if(socketInterface == null)
        {
            socketInterface = new SocketHandler();
        }
        return socketInterface;
    }

    @Override
    public boolean connect() {

        if (connectThread == null) {
            connectThread = new Thread(new ConnectThread());
        }
        if (!isConnected && !connectThread.isAlive()) {
            connectThread = new Thread(new ConnectThread());
            connectThread.start();
        }
        return isConnected;
     //   return true;
    }

    @Override
    public boolean disconnect() {
       new DisconnectThread().start();
       isConnected = false;
        return true;
    }

    @Override
    public void getMessage(IMessageNotifier notifier) {
        this.notifier = notifier;

        if (socket != null) {
            if (messageReceiveThread == null) {
                messageReceiveThread = new Thread(new GetMessageThread());
            }
            if (!messageReceiveThread.isAlive()) {
                messageReceiveThread.start();
            }

        }
    }

    @Override
    public void sendMessages(String message) {
        sendMessage = message;
        if (messageSendThread == null) {
            messageSendThread = new Thread(new SendMessageThread());
        }

        if (!messageSendThread.isAlive())
        {
            messageSendThread = new Thread(new SendMessageThread());
            messageSendThread.start();
        }

    }

    public class ConnectThread implements Runnable {
        @Override
        public void run() {

            try {

                socket = new Socket(Constants.IP_ADDRESS,Constants.PORT_NO);

                outputStream = new DataOutputStream(socket.getOutputStream());
                bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                inputStream = new DataInputStream(socket.getInputStream());
                isConnected = true;
                Log.d("socket","Connected");


            } catch (IOException e) {
                e.printStackTrace();
                Log.d("socket",e.getLocalizedMessage());


            }


        }
    }
    public class SendMessageThread extends Thread {
        @Override
        public void run() {
            try {
                if(outputStream != null)
                {
                    if(sendMessage.equalsIgnoreCase(Constants.CLOSE_SERVER))
                    {
                        disconnect();
                    }
                    outputStream.writeUTF(sendMessage);
                }
                else {
                    connect();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            super.run();

        }
    }
    public class DisconnectThread extends Thread{
        @Override
        public void run() {
            super.run();

            try {
                if(socket != null)
                {
                    sendMessages(Constants.CLOSE_SERVER);
                    socket.close();
                }
                Log.d("socket", "disconnect: ");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
    public class GetMessageThread implements Runnable {
        @Override
        public void run() {
                while (isConnected) {
                    try {
                        final String message = inputStream.readUTF();
                        if (message != null) {
                            notifier.newMessage(message);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }
                }


        }
    }

}
