package com.example.chatapp.socket;

import com.example.chatapp.common.Constants;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class SocketServer {

    private static final String TAG = "SocketServer";
    private static HashMap<UUID, Socket> socketHashMap;
    public static void main(String[] args) throws IOException {
        socketHashMap = new HashMap<>();
        try {
            startServer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void startServer() throws IOException {
        System.out.println("server started connect ");
        ServerSocket ss = new ServerSocket(6666);
        while (true) {
            Socket s = ss.accept();
            UUID uuid = UUID.randomUUID();
            socketHashMap.put(uuid, s);
            Client client = new Client(socketHashMap, uuid);
            System.out.println("new client connected");
            client.start();

        }
    }
       /* ServerSocket ss = new ServerSocket(6666);
        Socket s = ss.accept();
        DataInputStream din = new DataInputStream(s.getInputStream());
        DataOutputStream dout = new DataOutputStream(s.getOutputStream());
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        String str = "", str2 = "";
        int cnt=0;
        while (!str.equals("STOP")&&cnt<=100) {
            str = din.readUTF();
            System.out.println("Message : "+str);
            if(str!=null){
                dout.writeUTF(str);
                dout.flush();
            }else {
                str="";
                cnt++;
            }

        }
        din.close();
        s.close();
        ss.close();*/

}
