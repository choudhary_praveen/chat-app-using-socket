package com.example.chatapp.ui.chatRoom.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;
@Entity(tableName = "user_list")
public class UserListModel implements Parcelable {

    @NonNull
    @Expose
    @SerializedName("username")
    private String username;
    @Expose
    @SerializedName("id")
    @PrimaryKey
    @NonNull
    private UUID id;

    public UserListModel() {

    }


    protected UserListModel(Parcel in) {
        username = in.readString();
        id = UUID.fromString(in.readString());
    }

    public static final Creator<UserListModel> CREATOR = new Creator<UserListModel>() {
        @Override
        public UserListModel createFromParcel(Parcel in) {
            return new UserListModel(in);
        }

        @Override
        public UserListModel[] newArray(int size) {
            return new UserListModel[size];
        }
    };

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
        dest.writeString(id.toString());
    }
}


