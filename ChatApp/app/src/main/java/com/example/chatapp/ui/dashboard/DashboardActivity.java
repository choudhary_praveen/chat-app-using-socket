package com.example.chatapp.ui.dashboard;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.chatapp.R;
import com.example.chatapp.common.Constants;
import com.example.chatapp.databinding.ActivityDashboardBinding;
import com.example.chatapp.socket.IMessageNotifier;
import com.example.chatapp.socket.SocketHandler;
import com.example.chatapp.socket.SocketInterface;
import com.example.chatapp.ui.chatRoom.GroupChatRoom;
import com.example.chatapp.ui.chatRoom.adapter.GroupChatAdapter;
import com.example.chatapp.ui.chatRoom.model.ChatModel;
import com.example.chatapp.ui.userList.UserListActivity;
import com.google.gson.Gson;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.UUID;

public class DashboardActivity extends AppCompatActivity implements IMessageNotifier {
    ActivityDashboardBinding binding;
    SocketInterface socketInterface ;
    private ArrayList<ChatModel> msgList;
    private Gson gson;
    private Handler handler;
    private int msgCount = 0;
   // private ReadMessage readMessage;

    public DashboardActivity() {
        socketInterface = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDashboardBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initialize();





    }

    private void initialize() {
       socketInterface = SocketHandler.getInstance();
       binding.activeUser.setText("Not Connected");
       connectToServer();
       gson = new Gson();
       msgList = new ArrayList<>();

       binding.enterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (socketInterface.connect()) {
                    if (binding.clientName.getText().toString().length() > 0) {
                        ChatModel model = new ChatModel();

                         model.setUsername(binding.clientName.getText().toString());
                        model.setMessage("!@#$");
                      //  model.setSender(false);
                        model.setUuid(Constants.SENDER_UUID);
                        Gson gson = new Gson();
                        String jsonString = gson.toJson(model, ChatModel.class);
                        socketInterface.sendMessages(jsonString);
                        Intent intent = new Intent(DashboardActivity.this, UserListActivity.class);
                        intent.putExtra("username",binding.clientName.getText().toString());
                        intent.putExtra("msglist",msgList);
                        startActivity(intent);

                    }
                } else {
                    connectToServer();
                }
            }
        });
    }

    private void connectToServer() {
        socketInterface.connect();

       boolean connect = socketInterface.connect();
        Log.d("socket","conncetff");
        if(connect )
        { Toast.makeText(this, "Connected ", Toast.LENGTH_SHORT).show();
           binding.activeUser.setText("Connected");
            startReadingMessages();

        }
        else {
            new AlertDialog.Builder(this).setMessage("Failed To Connect. Try Again").setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    connectToServer();
                }
            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                 dialog.dismiss();
                }
            }).show();
        }
    }

    private void startReadingMessages() {
      socketInterface.getMessage(this);


    }

    @Override
    public void newMessage(String message) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(message != null && message.length()>0)
                {
                    ChatModel msgModel = gson.fromJson(message,ChatModel.class);

                    msgList.add(msgModel);
                    msgCount++;
                    if(msgCount>0)
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(msgCount>9)
                                {
                                    binding.activeUser.setText("Active User : 9+");
                                }
                                else {
                                    binding.activeUser.setText("Active User : " + msgCount);
                                }
                            }
                        });
                    }
                }
            }
        });
    }




}