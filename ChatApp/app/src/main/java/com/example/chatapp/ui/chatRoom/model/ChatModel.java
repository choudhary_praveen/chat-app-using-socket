package com.example.chatapp.ui.chatRoom.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.UUID;

public class ChatModel implements Parcelable {
    @Expose
    @SerializedName("username")
    private String username;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("uuid")
    private UUID uuid;
    @Expose
    @SerializedName("receiveruuid")
    private UUID receiver;
    @Expose
    @SerializedName("date")
    private String date;
    @Expose
    @SerializedName("userList")
    private HashMap<UUID,String> userList;
    @Expose
    @SerializedName("isPersonal")
    private boolean isPersonal = false;

    public ChatModel() {
    }


    protected ChatModel(Parcel in) {
        username = in.readString();
        message = in.readString();
        date = in.readString();
        isPersonal = in.readByte() != 0;
    }

    public static final Creator<ChatModel> CREATOR = new Creator<ChatModel>() {
        @Override
        public ChatModel createFromParcel(Parcel in) {
            return new ChatModel(in);
        }

        @Override
        public ChatModel[] newArray(int size) {
            return new ChatModel[size];
        }
    };

    public HashMap<UUID, String> getUserList() {
        return userList;
    }

    public void setUserList(HashMap<UUID, String> userList) {
        this.userList = userList;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public UUID getReceiver() {
        return receiver;
    }

    public void setReceiver(UUID receiver) {
        this.receiver = receiver;
    }


    public boolean isPersonal() {
        return isPersonal;
    }

    public void setPersonal(boolean personal) {
        isPersonal = personal;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
        dest.writeString(message);
        dest.writeString(date);
        dest.writeByte((byte) (isPersonal ? 1 : 0));
    }
}
