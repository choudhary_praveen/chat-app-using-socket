package com.example.chatapp.ui.chatRoom.adapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;

import com.example.chatapp.R;
import com.example.chatapp.common.Constants;
import com.example.chatapp.databinding.ActivityPersonalChatBinding;
import com.example.chatapp.socket.IMessageNotifier;
import com.example.chatapp.socket.SocketHandler;
import com.example.chatapp.socket.SocketInterface;
import com.example.chatapp.ui.chatRoom.model.ChatModel;
import com.example.chatapp.ui.chatRoom.model.UserListModel;
import com.example.chatapp.ui.userList.UserListActivity;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class PersonalChatActivity extends AppCompatActivity implements IMessageNotifier {
   private ActivityPersonalChatBinding binding;
    private ArrayList<ChatModel> recevedMsg ;
    private HashMap<UUID,String> userList;
    private GroupChatAdapter adapter;
    private String Username="";
    private Gson gson;
    private Handler handler;
    private Date currentTime;
    private UserListModel receiverUser;
    private boolean isPersonal = false;
    private SocketInterface socketInterface ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPersonalChatBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initialize();



    }

    private void initialize() {
        userList = new HashMap<>();
        receiverUser = new UserListModel();
        socketInterface = SocketHandler.getInstance();
        recevedMsg = new ArrayList<>();
        adapter = new GroupChatAdapter(this);
        currentTime = Calendar.getInstance().getTime();
        binding.chatView.setLayoutManager(new LinearLayoutManager(this));
        binding.chatView.setAdapter(adapter);

        gson = new Gson();
        handler = new Handler(Looper.getMainLooper());
        if(getIntent().getParcelableArrayExtra("msglist")!=null)
        {
            recevedMsg = getIntent().getParcelableExtra("msglist");
            adapter.setMessageList(recevedMsg);
            adapter.notifyDataSetChanged();
            binding.chatView.scrollToPosition(adapter.getItemCount()-1);
        }
        if(getIntent().getParcelableExtra("receiver")!=null)
        {
            isPersonal = true;

            receiverUser = getIntent().getParcelableExtra("receiver");
            binding.toolbarText.setText(receiverUser.getUsername());
        }
        if(UserListActivity.listHashMap.get(receiverUser.getId()) != null)
        {
            adapter.setMessageList(UserListActivity.listHashMap.get(receiverUser.getId()));
            adapter.notifyDataSetChanged();

        }
        //  readMessage = new ReadMessage();

        if(getIntent().getStringExtra("username")!=null)
        {
            Username = getIntent().getStringExtra("username");

           /*adapter.setUserName(Username);
            ChatModel model = new ChatModel();
            model.setUsername(getIntent().getStringExtra("username"));
            model.setMessage("!@#$");
            model.setDate(currentTime.toString());
            model.setUuid(Constants.SENDER_UUID);
            Gson gson = new Gson();
            String jsonString = gson.toJson(model, ChatModel.class);
            socketInterface.sendMessages(jsonString);*/

        }
        if(!isPersonal)
        {
            binding.toolbarText.setText("Broadcast Room");
            ChatModel model1 = new ChatModel();

            model1.setUsername(Username);
            model1.setMessage("!@#$");
            //  model.setSender(false);
            model1.setUuid(Constants.SENDER_UUID);
            Gson gson = new Gson();
            String jsonString1 = gson.toJson(model1, ChatModel.class);
            socketInterface.sendMessages(jsonString1);
        }


        binding.sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(binding.chat.getText().toString().length()>0)
                {
                    ChatModel model = new ChatModel();
                    model.setUsername(Username);
                    model.setUuid(Constants.SENDER_UUID);
                    model.setDate(currentTime.toString());
                    model.setMessage(binding.chat.getText().toString());
                    if(isPersonal)
                    {
                        model.setReceiver(receiverUser.getId());
                        model.setPersonal(true);
                    }
                    Gson gson = new Gson();
                    String jsonString = gson.toJson(model, ChatModel.class);
                    socketInterface.sendMessages(jsonString);
                    binding.chat.setText("");

                }
            }

        });
        binding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("userList",userList);
                setResult(Activity.RESULT_OK,intent);
                finish();
            }
        });

        socketInterface.getMessage(this::newMessage);


    }
    @Override
    public void newMessage(String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (message != null && message.length() > 0) {
                    Log.d("socketddd",message);
                    ChatModel msgModel = gson.fromJson(message, ChatModel.class);
                    //if(isPersonal)
                    if(!msgModel.getMessage().equalsIgnoreCase("@#$") && msgModel.isPersonal() )
                    {
                        adapter.setMessageModel(msgModel);
                        adapter.notifyDataSetChanged();
                        binding.chatView.scrollToPosition(adapter.getItemCount()-1);

                    }
                    else if(msgModel.getMessage().equalsIgnoreCase("@#$"))
                    {
                        userList.putAll(msgModel.getUserList());
                    }
                    if(!msgModel.isPersonal()&&!msgModel.getMessage().equalsIgnoreCase("@#$"))
                    {
                        if(UserListActivity.listHashMap.get(Constants.SENDER_UUID)== null)
                        {
                            UserListActivity.listHashMap.put(Constants.SENDER_UUID,new ArrayList<>());
                        }
                        UserListActivity.listHashMap.get(Constants.SENDER_UUID).add(msgModel);
                    }
                    else if(msgModel.isPersonal()&&!msgModel.getMessage().equalsIgnoreCase("@#$"))
                    {
                        if(!Constants.SENDER_UUID.equals(msgModel.getUuid()))
                        {
                            if(UserListActivity.listHashMap.get(msgModel.getUuid())==null)
                            {
                                UserListActivity.listHashMap.put(msgModel.getUuid(),new ArrayList<>());

                            }
                            UserListActivity.listHashMap.get(msgModel.getUuid()).add(msgModel);
                        }
                        else if(!Constants.SENDER_UUID.equals(msgModel.getReceiver()))
                        {
                            if(UserListActivity.listHashMap.get(msgModel.getReceiver())==null)
                            {
                                UserListActivity.listHashMap.put(msgModel.getReceiver(),new ArrayList<>());

                            }
                            UserListActivity.listHashMap.get(msgModel.getReceiver()).add(msgModel);
                        }
                    }

                }
            }
        });

    }

}