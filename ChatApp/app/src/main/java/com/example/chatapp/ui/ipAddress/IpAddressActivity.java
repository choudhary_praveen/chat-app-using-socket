package com.example.chatapp.ui.ipAddress;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.chatapp.common.Constants;
import com.example.chatapp.databinding.ActivityIpAddressBinding;
import com.example.chatapp.ui.dashboard.DashboardActivity;

public class IpAddressActivity extends AppCompatActivity {
    ActivityIpAddressBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityIpAddressBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initialize();
    }

    private void initialize() {

        binding.sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ipPattern = "^((0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)\\.){3}(0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)$";
                String ipAddress = binding.ipAddress.getText().toString();
                if(!ipAddress.matches(ipPattern))
                {

                    ShowToast("Enter Valid IP ADDRESS");
                      }
                else {
                  //  Constants.IP_ADDRESS = ipAddress;

                     }
                startActivity(new Intent(IpAddressActivity.this, DashboardActivity.class));



            }
        });
    }
    private void ShowToast(String error)
    {
        Toast.makeText(this,error,Toast.LENGTH_SHORT).show();

    }
}