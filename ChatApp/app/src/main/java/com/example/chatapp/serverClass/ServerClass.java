package com.example.chatapp.serverClass;

import com.example.chatapp.socket.Client;
import com.example.chatapp.ui.chatRoom.model.ChatModel;
import com.google.gson.Gson;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.UUID;

public class ServerClass {
    private static final String TAG = "SocketServer";
    private static HashMap<UUID, Socket> socketHashMap;
    private static HashMap<UUID, String> userList;

    public static void main(String[] args) throws IOException {
        socketHashMap = new HashMap<>();
        userList = new HashMap<>();
        try {
            startServer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void startServer() throws IOException {
        System.out.println("server started connect ");
        ServerSocket ss = new ServerSocket(6667);
        while (true) {
            Socket s = ss.accept();
            UUID uuid = null;
           // socketHashMap.put(uuid, s);
            DataInputStream din = new DataInputStream(s.getInputStream());
            if (din != null) {
                String str = "";
                try {
                    str = din.readUTF();
                    if (str != null) {
                      Gson gson = new Gson();
                      System.out.println("Srver" + str);
                        ChatModel chatModel = new ChatModel();
                        chatModel = gson.fromJson(str,ChatModel.class);
                        uuid = chatModel.getUuid();
                        socketHashMap.put(chatModel.getUuid(),s);
                        if(!userList.containsKey(chatModel.getUuid()))
                        userList.put(chatModel.getUuid(),chatModel.getUsername());
                        System.out.println(userList);
                    }
                }catch (EOFException e) {
                  //  list.remove(uuid);
                    System.out.println("removed element" + uuid);

                    break;
                }
                catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
               din = null;

                ClientClass client = new ClientClass(socketHashMap, uuid,userList);
                System.out.println("new client connected");
                client.start();

            }
        }
    }
}
