package com.example.chatapp.socket;

public interface IMessageNotifier {
    void newMessage(String message);
}
