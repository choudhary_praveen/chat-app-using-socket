package com.example.chatapp.serverClass;

import com.example.chatapp.ui.chatRoom.model.ChatModel;
import com.google.gson.Gson;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ClientClass extends Thread {
    private DataInputStream din;
    private DataOutputStream dout;
    private HashMap<UUID, Socket> map;
    private HashMap<UUID,String> list;
    private UUID uuid;


    ClientClass(HashMap<UUID, Socket> socket, UUID uuid,HashMap<UUID,String> userList) throws IOException {
        this.uuid = uuid;
        din = new DataInputStream(socket.get(uuid).getInputStream());
        dout = new DataOutputStream(socket.get(uuid).getOutputStream());
        this.map = socket;
        this.list = userList;
    }

    @Override
    public void run() {
        super.run();
        while (!map.get(uuid).isClosed()) {
            if (din != null) {
                String str = "";
                try {
                    str = din.readUTF();
                    if (str != null) {
                      //  System.out.println("writing message on server" + str);
                        for (Map.Entry mapElement : map.entrySet()) {
                            Socket socket = (Socket) mapElement.getValue();
                            UUID currentUuid = (UUID) mapElement.getKey();
                            if (!socket.isClosed()) {
                                Gson gson = new Gson();
                                ChatModel chatModel = new ChatModel();
                                chatModel = gson.fromJson(str, ChatModel.class);
                                if (chatModel.getMessage().equalsIgnoreCase("@#$")) {
                                    //System.out.println("user list" + str);
                                    chatModel.setUserList(list);
                                    // System.out.println("user list"+list);
                                    //  str = gson.toJson(chatModel,ChatModel.class);
                                    str = gson.toJson(chatModel, ChatModel.class);
                                    DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                                    dataOutputStream.writeUTF(str);
                                    dataOutputStream.flush();
                                    System.out.println("writing message " + str);
                                }
                                else if (chatModel.isPersonal() && chatModel.getReceiver().equals(currentUuid)) {
                                    //UUID uuid= chatModel.getUuid();
                                    //chatModel.setUuid(chatModel.getReceiver());
                                    //chatModel.setReceiver(uuid);
                                    str = gson.toJson(chatModel, ChatModel.class);
                                    DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                                    dataOutputStream.writeUTF(str);
                                    dataOutputStream.flush();
                                    System.out.println("writing personal recever " + str);

                                } else if (chatModel.isPersonal()  && chatModel.getUuid().equals(currentUuid)) {

                                    str = gson.toJson(chatModel, ChatModel.class);
                                    DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                                    dataOutputStream.writeUTF(str);
                                    dataOutputStream.flush();
                                    System.out.println("writing personal sender " + str);
                                }
                                else if(!chatModel.isPersonal()){
                                    str = gson.toJson(chatModel, ChatModel.class);
                                    DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                                    dataOutputStream.writeUTF(str);
                                    dataOutputStream.flush();
                                    System.out.println("writing message on server" + str);


                                }
                            }else {
                                if (!currentUuid.equals(uuid)) {
                                    map.remove(currentUuid);
                                    list.remove(currentUuid);

                                    break;
                                }
                                System.out.println("client disconnected");
                            }
                        }
                    }
                 //   System.out.println("" + str);
                    System.out.println("total clients connected" + map.size());


                }
                catch (NullPointerException e)
                {
                    
                }
                catch (EOFException e) {
                    list.remove(uuid);
                    System.out.println("removed element" + uuid);

                    break;
                }catch (Exception e) {
                    e.printStackTrace();
                    break;
                }



            }
        }
        try {
            din.close();
            dout.close();
        } catch (IOException e) {
            e.printStackTrace();

        }
    }
}
